package gv;

import flash.display.Graphics;
class GvSnap {
    private var time:Int;
    private var items:List<GvSnapItem>;
    public function new(time:Int) {
        this.time = time;
        items = new List<GvSnapItem>();
    }
    public function addItem(item:GvSnapItem):Void {
        items.add(item);
    }
    public function paint(g:Graphics, scale:Float, sx:Float, sy:Float):Void {
        for(item in items) {
            item.paint(g, scale, sx, sy);
        }
    }
    public function output():Void {
        for(item in items) {
            item.output();
        }
    }
    public function getTime():Int {
        return time;
    }
}