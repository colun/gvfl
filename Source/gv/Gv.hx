package gv;

class Gv {
    private static var enable_:Bool;
    public static function gvSetEnable(enable:Bool) {
        enable_ = enable;
    }
    public static function gvNewTime(time:Null<Int> = null):Void {
        if(!enable_) {
            return;
        }
        GvCore.gvNewTime(time);
    }
    public static function gvRollbackAll():Void {
        if(!enable_) {
            return;
        }
        GvCore.gvRollbackAll();
    }
    private static var colors:Array<UInt> = [0xFF0000, 0x00FF00, 0x0000FF, 0xFFFF00, 0x00FFFF, 0xFF00FF, 0xFF8000, 0xFF0080];
    public static function gvGetColorFromIndex(idx:Int):UInt {
        return colors[idx];
    }
    public static function gvCircle(x:Float, y:Float, r:Float = 0.5):GvSnapItem_Circle {
        var ret = new GvSnapItem_Circle(x, y, r);
        GvCore.gvAddItem(ret);
        return ret;
    }
}
