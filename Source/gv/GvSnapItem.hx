package gv;

import flash.display.Graphics;
interface GvSnapItem {
    function getMinX():Float;
    function getMinY():Float;
    function getMaxX():Float;
    function getMaxY():Float;
    function paint(g:Graphics, scale:Float, sx:Float, sy:Float):Void;
    function output():Void;
}