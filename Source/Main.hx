package;

import gv.Gv;
class Main {
    public static function main() {
        Gv.gvCircle(1.0, 1.0);
        Gv.gvCircle(2.0, 1.0).color(0);
        Gv.gvCircle(3.0, 1.0).color(1);
        Gv.gvCircle(4.0, 1.0).color(2);
        Gv.gvCircle(1.0, 2.0).rgb(255, 0, 0);
    }
}