package gv;

import flash.display.Graphics;

class GvSnapItem_Circle implements GvSnapItem {
    private var x:Float;
    private var y:Float;
    private var r:Float;
    private var c:UInt;
    public function new(x_:Float, y_:Float, r_:Float) {
        x = x_;
        y = y_;
        r = r_;
        c = 0;
    }
    public function rgb(r:Int, g:Int, b:Int):GvSnapItem_Circle {
        c = ((r&255)<<16) | ((g&255)<<8) | (b&255);
        return this;
    }
    public function color(cIdx:Int):GvSnapItem_Circle {
        c = Gv.gvGetColorFromIndex(cIdx);
        return this;
    }
    public function getMinX():Float {
        return x - r;
    }
    public function getMinY():Float {
        return y - r;
    }
    public function getMaxX():Float {
        return x + r;
    }
    public function getMaxY():Float {
        return y + r;
    }
    public function paint(g:Graphics, scale:Float, sx:Float, sy:Float):Void {
        var xx = (x+sx)*scale;
        var yy = (y+sy)*scale;
        var rr = r*scale;
        if(0<=xx+rr && 0<=yy+rr) {
            g.beginFill(c, 1);
            g.drawCircle(xx, yy, rr);
            g.endFill();
        }
    }
    public function output():Void {
    }
}