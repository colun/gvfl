package gv;


import flash.ui.Keyboard;
import flash.display.Graphics;
import flash.events.Event;
import flash.events.MouseEvent;
import flash.events.KeyboardEvent;
import flash.display.Sprite;


class GvPanel extends Sprite {
    var timeList:Array<Int> = null;
    var now:Int = 0;
    var nowSnap:GvSnap = null;
    var scale:Float = 1.0;
    var cx:Float = 0.0;
    var cy:Float = 0.0;
    var mx:Float = 0.0;
    var my:Float = 0.0;
    var cursorX:Float = 0.0;
    var cursorY:Float = 0.0;
    var myMouseX:Float = 0;
    var myMouseY:Float = 0;
    var autoMode:Bool = false;
    var autoModeCount:Int = 0;

    public function onNumpadKey(dx:Float, dy:Float) {
        var newCx = Math.min(Math.max(-mx, cx+dx*scale*0.25), mx);
        var newCy = Math.min(Math.max(-my, cy+dy*scale*0.25), my);
        if(cx!=newCx || cy!=newCy) {
            cx = newCx;
            cy = newCy;
            updateUI();
        }
    }
	public function new () {
        super ();
        stage.addEventListener(Event.RESIZE, function(event:Event):Void {
            updateUI();
        });
        stage.addEventListener(KeyboardEvent.KEY_DOWN, function(event:KeyboardEvent):Void {
            var keyCode:Int = event.keyCode;
            var keyLoc = event.keyLocation;
            switch(keyCode) {
                case Keyboard.LEFT:
                    autoMode = false;
                    if(1<=now) {
                        --now;
                        updateTime();
                    }
                case Keyboard.RIGHT:
                    autoMode = false;
                    if(timeList!=null && now<timeList.length-1) {
                        ++now;
                        updateTime();
                    }
                case Keyboard.UP:
                    updateSelf(null, false, 4, false, false);
                case Keyboard.DOWN:
                    updateSelf(null, false, -4, false, false);
                case Keyboard.NUMPAD_1:
                    onNumpadKey(-0.7, 0.7);
                case Keyboard.NUMPAD_2:
                    onNumpadKey(0, 1);
                case Keyboard.NUMPAD_3:
                    onNumpadKey(0.7, 0.7);
                case Keyboard.NUMPAD_4:
                    onNumpadKey(-1, 0);
                case Keyboard.NUMPAD_6:
                    onNumpadKey(1, 0);
                case Keyboard.NUMPAD_7:
                    onNumpadKey(-0.7, -0.7);
                case Keyboard.NUMPAD_8:
                    onNumpadKey(0, -1);
                case Keyboard.NUMPAD_9:
                    onNumpadKey(0.7, -0.7);
            }
        });
        var mouseDownFlag:Bool = false;
        stage.addEventListener(MouseEvent.MOUSE_MOVE, function(event:MouseEvent):Void {
            graphics.beginFill(0x987654);
            graphics.drawCircle(event.stageX, event.stageY, 3);
            graphics.endFill();
            myMouseX = event.stageX;
            myMouseY = event.stageY;
            updateSelf(null, event.buttonDown, 0, false, false);
        });
        stage.addEventListener(MouseEvent.MOUSE_UP, function(event:MouseEvent):Void {
            mouseDownFlag = false;
            graphics.beginFill(0xaaaaaa);
            graphics.drawCircle(event.stageX, event.stageY, 3);
            graphics.endFill();
            myMouseX = event.stageX;
            myMouseY = event.stageY;
            updateSelf(null, false, 0, false, false);
        });
        stage.addEventListener(MouseEvent.MOUSE_DOWN, function(event:MouseEvent):Void {
            mouseDownFlag = true;
            graphics.beginFill(0x444444);
            graphics.drawCircle(event.stageX, event.stageY, 3);
            graphics.endFill();
            myMouseX = event.stageX;
            myMouseY = event.stageY;
            updateSelf(null, false, 0, false, event.shiftKey);
        });
        stage.addEventListener(MouseEvent.MOUSE_WHEEL, function(event:MouseEvent):Void {
            graphics.beginFill(0xFF4444);
            graphics.drawCircle(event.stageX, event.stageY, 10);
            graphics.endFill();
            myMouseX = event.stageX;
            myMouseY = event.stageY;
            updateSelf(null, false, event.delta, false, false);
        });
        //stage.mouseEnabled = true;
        //this.mouseEnabled = true;
        Main.main();
        updateTimeList();
	}
    public function updateUI():Void {
        updateSelf(graphics, false, 0, false, false);
    }
    public function updateSelf(g:Graphics, mouseDown:Bool, zoom:Int, zoom2:Bool, shiftClick:Bool):Void {
        var width = Math.max(1, stage.stageWidth);
        var height = Math.max(1, stage.stageHeight);
        var dx = GvCore.getMaxX() - GvCore.getMinX();
        var dy = GvCore.getMaxY() - GvCore.getMinY();
        var maxD = Math.max(dx, dy);
        var scale, sx, sy;
        if(dx*height < dy*width) {
            my = (1-this.scale)*0.5;
            scale = height/(dy*this.scale);
            if(scale*dx<=width) {
                mx = 0;
            }
            else {
                mx = (dx-width/scale)/maxD * 0.5;
            }
        }
        else {
            mx = (1-this.scale)*0.5;
            scale = width/(dx*this.scale);
            if(scale*dy<=height) {
                my = 0;
            }
            else {
                my = (dy-height/scale)/maxD * 0.5;
            }
        }
        updateCenter();
        var beforeCursorX = cursorX;
        var beforeCursorY = cursorY;
        if(zoom2) {
            cx = (cursorX-(myMouseX-width*0.5)/scale-dx*0.5-GvCore.getMinX()) / maxD;
            cy = (cursorY-(myMouseY-height*0.5)/scale-dy*0.5-GvCore.getMinY()) / maxD;
            updateCenter();
            return;
        }
        cursorX = (myMouseX-width*0.5)/scale+dx*0.5+GvCore.getMinX()+maxD*cx;
        cursorY = (myMouseY-height*0.5)/scale+dy*0.5+GvCore.getMinY()+maxD*cy;
        if(mouseDown) {
            var dcx = cursorX - beforeCursorX;
            var dcy = cursorY - beforeCursorY;
            var oldCx = cx;
            var oldCy = cy;
            cx -= dcx/maxD;
            cy -= dcy/maxD;
            updateCenter();
            if(oldCx!=cx || oldCy!=cy) {
                updateUI();
                return;
            }
        }
        if(zoom!=0) {
            var newScale = Math.min(Math.max(0.01, this.scale * Math.pow(0.8, zoom*0.25)), 1.0);
            if(this.scale!=newScale) {
                this.scale = newScale;
                updateSelf(null, false, 0, true, false);
                updateUI();
                return;
            }
        }
        if(nowSnap==null) {
            return;
        }
        var time = nowSnap.getTime();
        if(shiftClick) {
            GvCore.sendInput(time, cursorX, cursorY);
        }
        var title:String;
        if(0<=myMouseX && 0<=myMouseY && GvCore.getMinX()<=cursorX && cursorX<=GvCore.getMaxX() && GvCore.getMinY()<=cursorY && cursorY<=GvCore.getMaxY()) {
            title = 'time ${time} ( ${now+1} / ${timeList.length} ) (${Std.int(cursorX+0.5)}, ${Std.int(cursorY+0.5)}) (${cursorX}, ${cursorY})';
        }
        else {
            title = 'time ${time} ( ${now+1} / ${timeList.length} )';
        }
        sx = (width/scale-dx)*0.5-GvCore.getMinX()-maxD*cx;
        sy = (height/scale-dy)*0.5-GvCore.getMinY()-maxD*cy;
        if(g!=null) {
            g.clear();
            nowSnap.paint(g, scale, sx, sy);
        }
    }
    private function updateCenter():Void {
        cx = Math.min(Math.max(-mx, cx), mx);
        cy = Math.min(Math.max(-my, cy), my);
    }
    private function updateTime():Void {
        if(timeList!=null && now<timeList.length) {
            var time:Int = timeList[now];
            if(now==timeList.length-1) {
                autoMode = true;
            }
            nowSnap = GvCore.getSnap(time);
            nowSnap.output();
            updateUI();
            var amc:Int = GvCore.getAutoModeCount();
            if(amc!=autoModeCount) {
                autoModeCount = amc;
                autoMode = true;
            }
            if(autoMode) {
                //TODO: autoModeTimer.restart();
            }
            return;
        }
        nowSnap = null;
    }
    private function updateTimeList():Void {
        var nowTime = (timeList!=null && now<timeList.length) ? timeList[now] : 0.0;
        timeList = GvCore.getTimeList();
        if(timeList!=null && 0<timeList.length) {
            var minDiff = Math.abs(nowTime - timeList[0]);
            now = 0;
            for(i in 1...timeList.length) {
                var diff = Math.abs(nowTime - timeList[i]);
                if(diff<minDiff) {
                    minDiff = diff;
                    now = i;
                }
            }
            updateTime();
        }
    }
}